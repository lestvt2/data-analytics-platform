# Successful modern data analytics platform design

---

![design](src/design.png "You like it?")
## Platform components and Task's
- Baremetal or Cloud ?
    - Consulting
    - Choose
- Linux (Centos7, Debian11)
    - Install utilities
        - python3
        - htop
        - iotop
        - iftop
        - strace
        - smarttools
    - Configure
        - ssh
        - users
        - mount points
        - network
        - performance
        - permissions
- InfluxDB (Virtual machine)
    - Install
    - Configure
        - Roles
        - Permissions
- Backup server
    - Configure
        - Network filesystem
            - nfs
            - s3 
        - Permissions
        - Rotation
- Greenplum
    - Configure host
        - sysctl
        - cgroups
        - ssh config
        - ssh cross keys
        - environment
        - .bashrc
    - Install
    - PXF
        - Compile
        - Configure
    - Roles
        - Groups
            - ro
            - rw
        - Users
            - ...
    - Permissions
        - Auto permissions for new objects
    - Resource groups
    - Maintenance
        - Backup
        - Cron (Need to develop)
            - Auto vacuum 
            - Auto reindex
            - Load average alerting
            - Locks alerting
            - PXF down alerting
            - Down alerting
            - Auto UP
            - Failover
- Postgres
    - Install & Configure
        - ETCD
        - HAProxy
        - Patroni
        - Bouncer
    - Tuning
        - Parameters
        - Autovacuum
    - Roles
        - Groups
            - ro
            - rw
        - Users
            - ...
    - Permissions
        - Auto permissions for new objects
    - Maintenance
        - Index health
        - Backup
- Airflow (Data pipelines)
    - Install
    - Configure Linux service's
        - webserver
        - scheduler
    - Service's down alerting
    - Install python libraries
    - Configure connections
    - Develop data pipelines
        - ...
- CI/CD
    - Configure gitlab-ci.yml
    - Register gitlab runner
- Telegraf on each host
    - Install
    - Configure
- Grafana (Virtual machine)
    - Install
    - Configure
    - Alerting
        - Health
        - Load Average
        - Disk
        - Memory
    - Dashboard
        - Greenplum
            - Connections
                - Status count
                - Top slow queries
            - Primary/ Mirror health
            - Locks
            - Network IO Bytes
            - Disk IO requests/ Bytes
            - Load average
        - Postgres
            - Like Greenplum
        - Hosts
            - Network
            - Disk
            - Load Average
            - Memory Usage
- Telegram Bot
    - Register bot
    - Create telegram alerting chats
        - Airflow data pipelines
        - Database Alerts
        - Security Alerts
        - Hosts Alerts
    - Add admin bot
    - Create Curl command for each chat
- Reporting
    - Install Superset or Redash
    - Configure Datasource's
    - Configure groups and permissions
    - Create dashboards
        - ...
- Security
    - SSH Alerting


## How to provide 

### MVP First
![kanban](src/kanban.jpeg "You like it?")

After that, we already have a minimal valuable product with a database, ETL pipeline and even a dashboard!

## What's Next ? 

### As you wish =)


It's good if we'll have in order of priority:

1. Backup
2. Monitoring
3. Alerting
4. CI/CD
5. Security

But we can also continue to connect data sources, build data pipelines and reports


## Cost's

On average, installation and configuration of each component takes from one to two weeks.

## Finish
If we a here, that mean we have a platform, it is strong, rock stable and production ready!

Now we can switch to support, response to critical errors should occur within an hour, for simple errors up to eight hours and from two to seven days for warnings
New date pipelines take on average one or two days.

## By choosing open source, you choose:
- For users
    - Privacy – Corporations often collect consumers' personal data for targeting and other purposes. For example, Microsoft was caught collecting the personal data of 300,000 Dutch civil servants through Office ProPlus without permission or documentation.
    - Long-term support - product development is driven by community requests, not corporate vision
    - Save money - no need to buy a product
    - Simple Feedback - Open Source developers and communities respond to incoming signals much faster than large companies
    - Thanks to Open Source, you can reduce the cost of porting many programs to all types of computers
- For developers
    - Adaptation of any software for the needs of your project 
    - Guaranteed developer community support
    - Ease of localization
- For corporations
    - Participation in open projects allows you to draw attention to your other programs, as well as develop your own ecosystem through the efforts of third-party developers.
    - The involvement of an open community in the company's projects makes it easier to find and hire new employees.
    - Buying open source companies allows you to keep talent within the company; external support for projects motivates developers to actively develop them.

![super](src/super.jpeg "You like it?")
